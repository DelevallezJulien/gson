/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.gson.internal;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A map of comparable keys to values. Unlike {@code TreeMap}, this class uses
 * insertion order for iteration order. Comparison order is only used as an
 * optimization for efficient insertion and removal.
 *
 * <p>This implementation was derived from Android 4.1's TreeMap and
 * LinkedHashMap classes.
 */
public final class LinkedHashTreeMap<K, V> extends TreeMap<K, V> implements Serializable {


	Comparator<? super K> comparator;
	Node<K, V>[] table;
	final Node<K, V> header;
	int threshold;

	/**
	 * Create a natural order, empty tree map whose keys must be mutually
	 * comparable and non-null.
	 */
	@SuppressWarnings("unchecked") // unsafe! this assumes K is comparable
	public LinkedHashTreeMap() {
		this((Comparator<? super K>) NATURAL_ORDER);
	}

	/**
	 * Create a tree map ordered by {@code comparator}. This map's keys may only
	 * be null if {@code comparator} permits.
	 *
	 * @param comparator the comparator to order elements with, or {@code null} to
	 *     use the natural ordering.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // unsafe! if comparator is null, this assumes K is comparable
	public LinkedHashTreeMap(Comparator<? super K> comparator) {
		this.comparator = comparator != null
				? comparator
						: (Comparator) NATURAL_ORDER;
		this.header = new Node<K, V>();
		this.table = new Node[16]; // TODO: sizing/resizing policies
		this.threshold = (table.length / 2) + (table.length / 4); // 3/4 capacity
	}

	public void clear() {
		Arrays.fill(table, null);
		size = 0;
		modCount++;

		// Clear all links to help GC
		Node<K, V> header = this.header;
		for (Node<K, V> e = header.next; e != header; ) {
			Node<K, V> next = e.next;
			e.next = e.prev = null;
			e = next;
		}

		header.next = header.prev = header;
	}

	/**
	 * Returns the node at or adjacent to the given key, creating it if requested.
	 *
	 * @throws ClassCastException if {@code key} and the tree's keys aren't
	 *     mutually comparable.
	 */
	public Node<K, V> find(K key, boolean create) {
		Comparator<? super K> comparator = this.comparator;
		Node<K, V>[] table = this.table;
		int hash = secondaryHash(key.hashCode());
		int index = hash & (table.length - 1);
		Node<K, V> nearest = table[index];
		int comparison = 0;

		if (nearest != null) {
			// Micro-optimization: avoid polymorphic calls to Comparator.compare().
			@SuppressWarnings("unchecked") // Throws a ClassCastException below if there's trouble.
			Comparable<Object> comparableKey = (comparator == NATURAL_ORDER)
			? (Comparable<Object>) key
					: null;

			while (true) {
				comparison = (comparableKey != null)
						? comparableKey.compareTo(nearest.key)
								: comparator.compare(key, nearest.key);

						// We found the requested key.
						if (comparison == 0) {
							return nearest;
						}

						// If it exists, the key is in a subtree. Go deeper.
						Node<K, V> child = (comparison < 0) ? nearest.left : nearest.right;
						if (child == null) {
							break;
						}

						nearest = child;
			}
		}

		// The key doesn't exist in this tree.
		if (!create) {
			return null;
		}

		// Create the node and add it to the tree or the table.
		Node<K, V> header = this.header;
		Node<K, V> created;
		if (nearest == null) {
			// Check that the value is comparable if we didn't do any comparisons.
			if (comparator == NATURAL_ORDER && !(key instanceof Comparable)) {
				throw new ClassCastException(key.getClass().getName() + " is not Comparable");
			}
			created = new Node<K, V>(nearest, key, hash, header, header.prev);
			table[index] = created;
		} else {
			created = new Node<K, V>(nearest, key, hash, header, header.prev);
			if (comparison < 0) { // nearest.key is higher
				nearest.left = created;
			} else { // comparison > 0, nearest.key is lower
				nearest.right = created;
			}
			rebalance(nearest, true);
		}

		if (size++ > threshold) {
			doubleCapacity();
		}
		modCount++;

		return created;
	}

	/**
	 * Applies a supplemental hash function to a given hashCode, which defends
	 * against poor quality hash functions. This is critical because HashMap
	 * uses power-of-two length hash tables, that otherwise encounter collisions
	 * for hashCodes that do not differ in lower or upper bits.
	 */
	private static int secondaryHash(int h) {
		// Doug Lea's supplemental hash function
		h ^= (h >>> 20) ^ (h >>> 12);
		return h ^ (h >>> 7) ^ (h >>> 4);
	}

	protected void replaceInParent(Node<K, V> node, Node<K, V> replacement) {
		Node<K, V> parent = node.parent;
		node.parent = null;
		if (replacement != null) {
			replacement.parent = parent;
		}

		if (parent != null) {
			if (parent.left == node) {
				parent.left = replacement;
			} else {
				assert (parent.right == node);
				parent.right = replacement;
			}
		} else {
			int index = node.hash & (table.length - 1);
			table[index] = replacement;
		}
	}

	private EntrySet entrySet;
	private KeySet keySet;

	@Override public Set<Entry<K, V>> entrySet() {
		EntrySet result = entrySet;
		return result != null ? result : (entrySet = new EntrySet());
	}

	@Override public Set<K> keySet() {
		KeySet result = keySet;
		return result != null ? result : (keySet = new KeySet());
	}

	private void doubleCapacity() {
		table = doubleCapacity(table);
		threshold = (table.length / 2) + (table.length / 4); // 3/4 capacity
	}

	/**
	 * Returns a new array containing the same nodes as {@code oldTable}, but with
	 * twice as many trees, each of (approximately) half the previous size.
	 */
	static <K, V> Node<K, V>[] doubleCapacity(Node<K, V>[] oldTable) {
		// TODO: don't do anything if we're already at MAX_CAPACITY
		int oldCapacity = oldTable.length;
		@SuppressWarnings("unchecked") // Arrays and generics don't get along.
		Node<K, V>[] newTable = new Node[oldCapacity * 2];
		AvlIterator<K, V> iterator = new AvlIterator<K, V>();
		AvlBuilder<K, V> leftBuilder = new AvlBuilder<K, V>();
		AvlBuilder<K, V> rightBuilder = new AvlBuilder<K, V>();

		// Split each tree into two trees.
		for (int i = 0; i < oldCapacity; i++) {
			Node<K, V> root = oldTable[i];
			if (root == null) {
				continue;
			}

			// Compute the sizes of the left and right trees.
			iterator.reset(root);
			int leftSize = 0;
			int rightSize = 0;
			for (Node<K, V> node; (node = iterator.next()) != null; ) {
				if ((node.hash & oldCapacity) == 0) {
					leftSize++;
				} else {
					rightSize++;
				}
			}

			// Split the tree into two.
			leftBuilder.reset(leftSize);
			rightBuilder.reset(rightSize);
			iterator.reset(root);
			for (Node<K, V> node; (node = iterator.next()) != null; ) {
				if ((node.hash & oldCapacity) == 0) {
					leftBuilder.add(node);
				} else {
					rightBuilder.add(node);
				}
			}

			// Populate the enlarged array with these new roots.
			newTable[i] = leftSize > 0 ? leftBuilder.root() : null;
			newTable[i + oldCapacity] = rightSize > 0 ? rightBuilder.root() : null;
		}
		return newTable;
	}

	/**
	 * Walks an AVL tree in iteration order. Once a node has been returned, its
	 * left, right and parent links are <strong>no longer used</strong>. For this
	 * reason it is safe to transform these links as you walk a tree.
	 *
	 * <p><strong>Warning:</strong> this iterator is destructive. It clears the
	 * parent node of all nodes in the tree. It is an error to make a partial
	 * iteration of a tree.
	 */
	static class AvlIterator<K, V> {
		/** This stack is a singly linked list, linked by the 'parent' field. */
		private Node<K, V> stackTop;

		void reset(Node<K, V> root) {
			Node<K, V> stackTop = null;
			for (Node<K, V> n = root; n != null; n = n.left) {
				n.parent = stackTop;
				stackTop = n; // Stack push.
			}
			this.stackTop = stackTop;
		}

		public Node<K, V> next() {
			Node<K, V> stackTop = this.stackTop;
			if (stackTop == null) {
				return null;
			}
			Node<K, V> result = stackTop;
			stackTop = result.parent;
			result.parent = null;
			for (Node<K, V> n = result.right; n != null; n = n.left) {
				n.parent = stackTop;
				stackTop = n; // Stack push.
			}
			this.stackTop = stackTop;
			return result;
		}
	}

	/**
	 * Builds AVL trees of a predetermined size by accepting nodes of increasing
	 * value. To use:
	 * <ol>
	 *   <li>Call {@link #reset} to initialize the target size <i>size</i>.
	 *   <li>Call {@link #add} <i>size</i> times with increasing values.
	 *   <li>Call {@link #root} to get the root of the balanced tree.
	 * </ol>
	 *
	 * <p>The returned tree will satisfy the AVL constraint: for every node
	 * <i>N</i>, the height of <i>N.left</i> and <i>N.right</i> is different by at
	 * most 1. It accomplishes this by omitting deepest-level leaf nodes when
	 * building trees whose size isn't a power of 2 minus 1.
	 *
	 * <p>Unlike rebuilding a tree from scratch, this approach requires no value
	 * comparisons. Using this class to create a tree of size <i>S</i> is
	 * {@code O(S)}.
	 */
	final static class AvlBuilder<K, V> {
		/** This stack is a singly linked list, linked by the 'parent' field. */
		private Node<K, V> stack;
		private int leavesToSkip;
		private int leavesSkipped;
		private int size;

		void reset(int targetSize) {
			// compute the target tree size. This is a power of 2 minus one, like 15 or 31.
			int treeCapacity = Integer.highestOneBit(targetSize) * 2 - 1;
			leavesToSkip = treeCapacity - targetSize;
			size = 0;
			leavesSkipped = 0;
			stack = null;
		}

		void add(Node<K, V> node) {
			node.left = node.parent = node.right = null;
			node.height = 1;

			// Skip a leaf if necessary.
			if (leavesToSkip > 0 && (size & 1) == 0) {
				size++;
				leavesToSkip--;
				leavesSkipped++;
			}

			node.parent = stack;
			stack = node; // Stack push.
			size++;

			// Skip a leaf if necessary.
			if (leavesToSkip > 0 && (size & 1) == 0) {
				size++;
				leavesToSkip--;
				leavesSkipped++;
			}

			/*
			 * Combine 3 nodes into subtrees whenever the size is one less than a
			 * multiple of 4. For example we combine the nodes A, B, C into a
			 * 3-element tree with B as the root.
			 *
			 * Combine two subtrees and a spare single value whenever the size is one
			 * less than a multiple of 8. For example at 8 we may combine subtrees
			 * (A B C) and (E F G) with D as the root to form ((A B C) D (E F G)).
			 *
			 * Just as we combine single nodes when size nears a multiple of 4, and
			 * 3-element trees when size nears a multiple of 8, we combine subtrees of
			 * size (N-1) whenever the total size is 2N-1 whenever N is a power of 2.
			 */
			for (int scale = 4; (size & scale - 1) == scale - 1; scale *= 2) {
				if (leavesSkipped == 0) {
					// Pop right, center and left, then make center the top of the stack.
					Node<K, V> right = stack;
					Node<K, V> center = right.parent;
					Node<K, V> left = center.parent;
					center.parent = left.parent;
					stack = center;
					// Construct a tree.
					center.left = left;
					center.right = right;
					center.height = right.height + 1;
					left.parent = center;
					right.parent = center;
				} else if (leavesSkipped == 1) {
					// Pop right and center, then make center the top of the stack.
					Node<K, V> right = stack;
					Node<K, V> center = right.parent;
					stack = center;
					// Construct a tree with no left child.
					center.right = right;
					center.height = right.height + 1;
					right.parent = center;
					leavesSkipped = 0;
				} else if (leavesSkipped == 2) {
					leavesSkipped = 0;
				}
			}
		}

		Node<K, V> root() {
			Node<K, V> stackTop = this.stack;
			if (stackTop.parent != null) {
				throw new IllegalStateException();
			}
			return stackTop;
		}
	}

	private abstract class LinkedTreeMapIterator<T> implements Iterator<T> {
		Node<K, V> next = header.next;
		Node<K, V> lastReturned = null;
		int expectedModCount = modCount;

		LinkedTreeMapIterator() {
		}

		public final boolean hasNext() {
			return next != header;
		}

		final Node<K, V> nextNode() {
			Node<K, V> e = next;
			if (e == header) {
				throw new NoSuchElementException();
			}
			if (modCount != expectedModCount) {
				throw new ConcurrentModificationException();
			}
			next = e.next;
			return lastReturned = e;
		}

		public final void remove() {
			if (lastReturned == null) {
				throw new IllegalStateException();
			}
			removeInternal(lastReturned, true);
			lastReturned = null;
			expectedModCount = modCount;
		}
	}

	final class EntrySet extends AbstractSet<Entry<K, V>> {
		@Override public int size() {
			return size;
		}

		@Override public Iterator<Entry<K, V>> iterator() {
			return new LinkedTreeMapIterator<Entry<K, V>>() {
				public Entry<K, V> next() {
					return nextNode();
				}
			};
		}

		@Override public boolean contains(Object o) {
			return o instanceof Entry && findByEntry((Entry<?, ?>) o) != null;
		}

		@Override public boolean remove(Object o) {
			if (!(o instanceof Entry)) {
				return false;
			}

			Node<K, V> node = findByEntry((Entry<?, ?>) o);
			if (node == null) {
				return false;
			}
			removeInternal(node, true);
			return true;
		}

		@Override public void clear() {
			LinkedHashTreeMap.this.clear();
		}
	}

	final class KeySet extends AbstractSet<K> {
		@Override public int size() {
			return size;
		}

		@Override public Iterator<K> iterator() {
			return new LinkedTreeMapIterator<K>() {
				public K next() {
					return nextNode().key;
				}
			};
		}

		@Override public boolean contains(Object o) {
			return containsKey(o);
		}

		@Override public boolean remove(Object key) {
			return removeInternalByKey(key) != null;
		}

		@Override public void clear() {
			LinkedHashTreeMap.this.clear();
		}
	}

	/**
	 * If somebody is unlucky enough to have to serialize one of these, serialize
	 * it as a LinkedHashMap so that they won't need Gson on the other side to
	 * deserialize it. Using serialization defeats our DoS defence, so most apps
	 * shouldn't use it.
	 */
	private Object writeReplace() throws ObjectStreamException {
		return new LinkedHashMap<K, V>(this);
	}
}
