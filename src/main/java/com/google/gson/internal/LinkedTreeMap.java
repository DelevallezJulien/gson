/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.gson.internal;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A map of comparable keys to values. Unlike {@code TreeMap}, this class uses
 * insertion order for iteration order. Comparison order is only used as an
 * optimization for efficient insertion and removal.
 *
 * <p>This implementation was derived from Android 4.1's TreeMap class.
 */
public final class LinkedTreeMap<K, V> extends TreeMap<K, V> implements Serializable {

	Comparator<? super K> comparator;
	Node<K, V> root;

	// Used to preserve iteration order
	final Node<K, V> header = new Node<K, V>();

	/**
	 * Create a natural order, empty tree map whose keys must be mutually
	 * comparable and non-null.
	 */
	@SuppressWarnings("unchecked") // unsafe! this assumes K is comparable
	public LinkedTreeMap() {
		this((Comparator<? super K>) NATURAL_ORDER);
	}

	/**
	 * Create a tree map ordered by {@code comparator}. This map's keys may only
	 * be null if {@code comparator} permits.
	 *
	 * @param comparator the comparator to order elements with, or {@code null} to
	 *     use the natural ordering.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // unsafe! if comparator is null, this assumes K is comparable
	public LinkedTreeMap(Comparator<? super K> comparator) {
		this.comparator = comparator != null
				? comparator
						: (Comparator) NATURAL_ORDER;
	}

	@Override public void clear() {
		root = null;
		size = 0;
		modCount++;

		// Clear iteration order
		Node<K, V> header = this.header;
		header.next = header.prev = header;
	}

	/**
	 * Returns the node at or adjacent to the given key, creating it if requested.
	 *
	 * @throws ClassCastException if {@code key} and the tree's keys aren't
	 *     mutually comparable.
	 */
	public Node<K, V> find(K key, boolean create) {
		Comparator<? super K> comparator = this.comparator;
		Node<K, V> nearest = root;
		int comparison = 0;

		if (nearest != null) {
			// Micro-optimization: avoid polymorphic calls to Comparator.compare().
			@SuppressWarnings("unchecked") // Throws a ClassCastException below if there's trouble.
			Comparable<Object> comparableKey = (comparator == NATURAL_ORDER)
			? (Comparable<Object>) key
					: null;

			while (true) {
				comparison = (comparableKey != null)
						? comparableKey.compareTo(nearest.key)
								: comparator.compare(key, nearest.key);

						// We found the requested key.
						if (comparison == 0) {
							return nearest;
						}

						// If it exists, the key is in a subtree. Go deeper.
						Node<K, V> child = (comparison < 0) ? nearest.left : nearest.right;
						if (child == null) {
							break;
						}

						nearest = child;
			}
		}

		// The key doesn't exist in this tree.
		if (!create) {
			return null;
		}

		// Create the node and add it to the tree or the table.
		Node<K, V> header = this.header;
		Node<K, V> created;
		if (nearest == null) {
			// Check that the value is comparable if we didn't do any comparisons.
			if (comparator == NATURAL_ORDER && !(key instanceof Comparable)) {
				throw new ClassCastException(key.getClass().getName() + " is not Comparable");
			}
			created = new Node<K, V>(nearest, key, header, header.prev);
			root = created;
		} else {
			created = new Node<K, V>(nearest, key, header, header.prev);
			if (comparison < 0) { // nearest.key is higher
				nearest.left = created;
			} else { // comparison > 0, nearest.key is lower
				nearest.right = created;
			}
			rebalance(nearest, true);
		}
		size++;
		modCount++;

		return created;
	}

	protected void replaceInParent(Node<K, V> node, Node<K, V> replacement) {
		Node<K, V> parent = node.parent;
		node.parent = null;
		if (replacement != null) {
			replacement.parent = parent;
		}

		if (parent != null) {
			if (parent.left == node) {
				parent.left = replacement;
			} else {
				assert (parent.right == node);
				parent.right = replacement;
			}
		} else {
			root = replacement;
		}
	}

	private EntrySet entrySet;
	private KeySet keySet;

	@Override public Set<Entry<K, V>> entrySet() {
		EntrySet result = entrySet;
		return result != null ? result : (entrySet = new EntrySet());
	}

	@Override public Set<K> keySet() {
		KeySet result = keySet;
		return result != null ? result : (keySet = new KeySet());
	}

	private abstract class LinkedTreeMapIterator<T> implements Iterator<T> {
		Node<K, V> next = header.next;
		Node<K, V> lastReturned = null;
		int expectedModCount = modCount;

		LinkedTreeMapIterator() {
		}

		public final boolean hasNext() {
			return next != header;
		}

		final Node<K, V> nextNode() {
			Node<K, V> e = next;
			if (e == header) {
				throw new NoSuchElementException();
			}
			if (modCount != expectedModCount) {
				throw new ConcurrentModificationException();
			}
			next = e.next;
			return lastReturned = e;
		}

		public final void remove() {
			if (lastReturned == null) {
				throw new IllegalStateException();
			}
			removeInternal(lastReturned, true);
			lastReturned = null;
			expectedModCount = modCount;
		}
	}

	class EntrySet extends AbstractSet<Entry<K, V>> {
		@Override public int size() {
			return size;
		}

		@Override public Iterator<Entry<K, V>> iterator() {
			return new LinkedTreeMapIterator<Entry<K, V>>() {
				public Entry<K, V> next() {
					return nextNode();
				}
			};
		}

		@Override public boolean contains(Object o) {
			return o instanceof Entry && findByEntry((Entry<?, ?>) o) != null;
		}

		@Override public boolean remove(Object o) {
			if (!(o instanceof Entry)) {
				return false;
			}

			Node<K, V> node = findByEntry((Entry<?, ?>) o);
			if (node == null) {
				return false;
			}
			removeInternal(node, true);
			return true;
		}

		@Override public void clear() {
			LinkedTreeMap.this.clear();
		}
	}

	final class KeySet extends AbstractSet<K> {
		@Override public int size() {
			return size;
		}

		@Override public Iterator<K> iterator() {
			return new LinkedTreeMapIterator<K>() {
				public K next() {
					return nextNode().key;
				}
			};
		}

		@Override public boolean contains(Object o) {
			return containsKey(o);
		}

		@Override public boolean remove(Object key) {
			return removeInternalByKey(key) != null;
		}

		@Override public void clear() {
			LinkedTreeMap.this.clear();
		}
	}

	/**
	 * If somebody is unlucky enough to have to serialize one of these, serialize
	 * it as a LinkedHashMap so that they won't need Gson on the other side to
	 * deserialize it. Using serialization defeats our DoS defence, so most apps
	 * shouldn't use it.
	 */
	private Object writeReplace() throws ObjectStreamException {
		return new LinkedHashMap<K, V>(this);
	}
}
