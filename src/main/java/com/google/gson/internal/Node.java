package com.google.gson.internal;

import java.util.Map.Entry;

public class Node <K, V> implements Entry<K, V> {

	Node<K, V> parent;
	Node<K, V> left;
	Node<K, V> right;
	Node<K, V> next;
	Node<K, V> prev;
	final K key;
	final int hash;
	V value;
	int height;

	Node() {
		key = null;
		hash = -1;
		next = prev = this;
	}
	
	Node(Node<K, V> parent, K key, int hash, Node<K, V> next, Node<K, V> prev) {
		this.parent = parent;
		this.key = key;
		this.hash = hash;
		this.height = 1;
		this.next = next;
		this.prev = prev;
		prev.next = this;
		next.prev = this;
	}

	Node(Node<K, V> parent, K key, Node<K, V> next, Node<K, V> prev) {
		this.parent = parent;
		this.key = key;
		this.height = 1;
		this.next = next;
		this.prev = prev;
		prev.next = this;
		next.prev = this;
		hash=-1;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public V setValue(V value) {
		V oldValue = this.value;
		this.value = value;
		return oldValue;
	}

	public boolean equals(Object o) {
		if (o instanceof Entry) {
			Entry other = (Entry) o;
			return (key == null ? other.getKey() == null : key.equals(other.getKey()))
					&& (value == null ? other.getValue() == null : value.equals(other.getValue()));
		}
		return false;
	}

	public int hashCode() {
		return (key == null ? 0 : key.hashCode())
				^ (value == null ? 0 : value.hashCode());
	}

	public String toString() {
		return key + "=" + value;
	}

	public Node<K, V> first() {
		Node<K, V> node = this;
		Node<K, V> child = node.left;
		while (child != null) {
			node = child;
			child = node.left;
		}
		return node;
	}

	public Node<K, V> last() {
		Node<K, V> node = this;
		Node<K, V> child = node.right;
		while (child != null) {
			node = child;
			child = node.right;
		}
		return node;
	}

}
