package com.google.gson.internal;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Comparator;

public abstract class TreeMap <K, V> extends AbstractMap<K, V> implements Serializable {

	int size = 0;
	int modCount = 0;

	protected static final Comparator<Comparable> NATURAL_ORDER = new Comparator<Comparable>() {
		public int compare(Comparable a, Comparable b) {
			return a.compareTo(b);
		}
	};

	@Override 
	public int size() {
		return size;
	}

	@Override 
	public V get(Object key) {
		Node<K, V> node = findByObject(key);
		return node != null ? node.value : null;
	}

	@Override 
	public V put(K key, V value) {
		if (key == null) {
			throw new NullPointerException("key == null");
		}
		Node<K, V> created = find(key, true);
		V result = created.value;
		created.value = value;
		return result;
	}

	@Override 
	public boolean containsKey(Object key) {
		return findByObject(key) != null;
	}

	Node<K, V> findByObject(Object key) {
		try {
			return key != null ? find((K) key, false) : null;
		} catch (ClassCastException e) {
			return null;
		}
	}


	public abstract void clear();
	public abstract Node<K, V> find(K key, boolean create);

	@Override 
	public V remove(Object key) {
		Node<K, V> node = removeInternalByKey(key);
		return node != null ? node.value : null;
	}

	Node<K, V> findByEntry(Entry<?, ?> entry) {
		Node<K, V> mine = findByObject(entry.getKey());
		boolean valuesEqual = mine != null && equal(mine.value, entry.getValue());
		return valuesEqual ? mine : null;
	}

	private boolean equal(Object a, Object b) {
		return a == b || (a != null && a.equals(b));
	}

	public void removeInternal(Node<K, V> node, boolean unlink) {
		if (unlink) {
			node.prev.next = node.next;
			node.next.prev = node.prev;
			node.next = node.prev = null; // Help the GC (for performance)
		}

		Node<K, V> left = node.left;
		Node<K, V> right = node.right;
		Node<K, V> originalParent = node.parent;
		if (left != null && right != null) {

			/*
			 * To remove a node with both left and right subtrees, move an
			 * adjacent node from one of those subtrees into this node's place.
			 *
			 * Removing the adjacent node may change this node's subtrees. This
			 * node may no longer have two subtrees once the adjacent node is
			 * gone!
			 */

			Node<K, V> adjacent = (left.height > right.height) ? left.last() : right.first();
			removeInternal(adjacent, false); // takes care of rebalance and size--

			int leftHeight = 0;
			left = node.left;
			if (left != null) {
				leftHeight = left.height;
				adjacent.left = left;
				left.parent = adjacent;
				node.left = null;
			}
			int rightHeight = 0;
			right = node.right;
			if (right != null) {
				rightHeight = right.height;
				adjacent.right = right;
				right.parent = adjacent;
				node.right = null;
			}
			adjacent.height = Math.max(leftHeight, rightHeight) + 1;
			replaceInParent(node, adjacent);
			return;
		} else if (left != null) {
			replaceInParent(node, left);
			node.left = null;
		} else if (right != null) {
			replaceInParent(node, right);
			node.right = null;
		} else {
			replaceInParent(node, null);
		}

		rebalance(originalParent, false);
		size--;
		modCount++;
	}

	protected Node<K, V> removeInternalByKey(Object key) {
		Node<K, V> node = findByObject(key);
		if (node != null) {
			removeInternal(node, true);
		}
		return node;
	}

	protected abstract void replaceInParent(Node<K, V> node, Node<K, V> replacement);

	protected void rebalance(Node<K, V> unbalanced, boolean insert) {
		for (Node<K, V> node = unbalanced; node != null; node = node.parent) {
			Node<K, V> left = node.left;
			Node<K, V> right = node.right;
			int leftHeight = left != null ? left.height : 0;
			int rightHeight = right != null ? right.height : 0;

			int delta = leftHeight - rightHeight;
			if (delta == -2) {
				Node<K, V> rightLeft = right.left;
				Node<K, V> rightRight = right.right;
				int rightRightHeight = rightRight != null ? rightRight.height : 0;
				int rightLeftHeight = rightLeft != null ? rightLeft.height : 0;

				int rightDelta = rightLeftHeight - rightRightHeight;
				if (rightDelta == -1 || (rightDelta == 0 && !insert)) {
					rotateLeft(node); // AVL right right
				} else {
					assert (rightDelta == 1);
					rotateRight(right); // AVL right left
					rotateLeft(node);
				}
				if (insert) {
					break; // no further rotations will be necessary
				}

			} else if (delta == 2) {
				Node<K, V> leftLeft = left.left;
				Node<K, V> leftRight = left.right;
				int leftRightHeight = leftRight != null ? leftRight.height : 0;
				int leftLeftHeight = leftLeft != null ? leftLeft.height : 0;

				int leftDelta = leftLeftHeight - leftRightHeight;
				if (leftDelta == 1 || (leftDelta == 0 && !insert)) {
					rotateRight(node); // AVL left left
				} else {
					assert (leftDelta == -1);
					rotateLeft(left); // AVL left right
					rotateRight(node);
				}
				if (insert) {
					break; // no further rotations will be necessary
				}

			} else if (delta == 0) {
				node.height = leftHeight + 1; // leftHeight == rightHeight
				if (insert) {
					break; // the insert caused balance, so rebalancing is done!
				}

			} else {
				assert (delta == -1 || delta == 1);
				node.height = Math.max(leftHeight, rightHeight) + 1;
				if (!insert) {
					break; // the height hasn't changed, so rebalancing is done!
				}
			}
		}
	}

	private void rotateLeft(Node<K, V> root) {
		Node<K, V> left = root.left;
		Node<K, V> pivot = root.right;
		Node<K, V> pivotLeft = pivot.left;
		Node<K, V> pivotRight = pivot.right;

		// move the pivot's left child to the root's right
		root.right = pivotLeft;
		if (pivotLeft != null) {
			pivotLeft.parent = root;
		}

		replaceInParent(root, pivot);

		// move the root to the pivot's left
		pivot.left = root;
		root.parent = pivot;

		// fix heights
		root.height = Math.max(left != null ? left.height : 0,
				pivotLeft != null ? pivotLeft.height : 0) + 1;
		pivot.height = Math.max(root.height,
				pivotRight != null ? pivotRight.height : 0) + 1;
	}

	private void rotateRight(Node<K, V> root) {
		Node<K, V> pivot = root.left;
		Node<K, V> right = root.right;
		Node<K, V> pivotLeft = pivot.left;
		Node<K, V> pivotRight = pivot.right;

		// move the pivot's right child to the root's left
		root.left = pivotRight;
		if (pivotRight != null) {
			pivotRight.parent = root;
		}

		replaceInParent(root, pivot);

		// move the root to the pivot's right
		pivot.right = root;
		root.parent = pivot;

		// fixup heights
		root.height = Math.max(right != null ? right.height : 0,
				pivotRight != null ? pivotRight.height : 0) + 1;
		pivot.height = Math.max(root.height,
				pivotLeft != null ? pivotLeft.height : 0) + 1;
	}
}
